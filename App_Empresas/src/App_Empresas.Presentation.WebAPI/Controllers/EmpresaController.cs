﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using App_Empresas.Application.Interfaces;
using App_Empresas.Application.ViewModels;

namespace App_Empresas.Presentation.WebAPI.Controllers
{
    public class EmpresaController : ApiController
    {
        private readonly IEmpresaAppService _empresaAppService;

        public EmpresaController(IEmpresaAppService empresaAppService)
        {
            _empresaAppService = empresaAppService;
        }

        //Retorna todas as empresas cadastradas
        public IQueryable<EmpresaViewModel> GetAllEmpresas()
        {
            var empresas = _empresaAppService.GetAll().AsQueryable();
            return empresas;
        }

        //Retorna todas as empresas filtradas por nome e tipo
        [ResponseType(typeof(EmpresaViewModel))]
        public IHttpActionResult GetAllEmpresasFilterByNameTipo(string name, string type)
        {
            var empresasFilter = _empresaAppService.GetAll_FilterByNameType(name, type);

            if(empresasFilter.Count() == 0)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            return Ok(empresasFilter);
        }

        //Retorna uma empresa específica
        [ResponseType(typeof(EmpresaViewModel))]
        public IHttpActionResult GetById(Guid id)
        {
            EmpresaViewModel empresaViewModel = _empresaAppService.GetById(id);
            if (empresaViewModel == null)
            {
                return NotFound();
            }

            return Ok(empresaViewModel);
        }

        //Modifica registro de empresa
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult UpdateEmpresa(EmpresaViewModel empresaViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }           

            _empresaAppService.Update(empresaViewModel);

            return StatusCode(HttpStatusCode.NoContent);
        }

        //Cadastra nova empresa
        [ResponseType(typeof(EmpresaViewModel))]
        [HttpPost]
        public IHttpActionResult AddEmpresa(EmpresaViewModel empresaViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }         

            try
            {
                _empresaAppService.Add(empresaViewModel);
            }
            catch (DbUpdateException)
            {
                if (EmpresaExists(empresaViewModel.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("Route1", new { id = empresaViewModel.id }, empresaViewModel);
        }

        //Remove registro de empresa
        [ResponseType(typeof(EmpresaViewModel))]
        [HttpDelete]
        public IHttpActionResult RemoveEmpresa(Guid id)
        {
            EmpresaViewModel empresaViewModel = _empresaAppService.GetById(id);
            if (empresaViewModel == null)
            {
                return NotFound();
            }

            _empresaAppService.Remove(id);


            return Ok(empresaViewModel);
        }

        private bool EmpresaExists(Guid id)
        {
            return _empresaAppService.GetAll().Count(e => e.id == id) > 0;
        }
    }
}