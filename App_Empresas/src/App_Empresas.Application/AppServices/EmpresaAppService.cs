﻿using App_Empresas.Application.Interfaces;
using App_Empresas.Application.ViewModels;
using App_Empresas.Domain.Entities;
using App_Empresas.Domain.Interfaces.Service;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Application.AppServices
{
    public class EmpresaAppService : AppService, IEmpresaAppService
    {
        public readonly IEmpresaService _empresaService;

        public EmpresaAppService(IEmpresaService empresaService)
        {
            _empresaService = empresaService;
        }

        public void Add(EmpresaViewModel obj)
        {
            var empresa = Mapper.Map<EmpresaViewModel, Empresa>(obj);
            BeginTransaction();
            _empresaService.Add(empresa);
            Commit();
        }

        public IEnumerable<EmpresaViewModel> GetAll()
        {
            var empresas =_empresaService.GetAll();
            var result = Mapper.Map<IEnumerable<Empresa>, IEnumerable<EmpresaViewModel>>(empresas);
            return result;
        }

        public IEnumerable<EmpresaViewModel> GetAll_FilterByNameType(string name, string tipo)
        {
            var empresasFilter = _empresaService.GetAll_FilterByNameType(name, tipo);
            var result = Mapper.Map<IEnumerable<Empresa>, IEnumerable<EmpresaViewModel>>(empresasFilter);
            return result;
        }

        public EmpresaViewModel GetById(Guid id)
        {
            var empresa = _empresaService.GetById(id);
            var result = Mapper.Map<Empresa, EmpresaViewModel>(empresa);
            return result;
        }

        public void Remove(Guid id)
        {
            BeginTransaction();
            _empresaService.Remove(id);
            Commit();
        }

        public void Update(EmpresaViewModel obj)
        {
            var empresa = Mapper.Map<EmpresaViewModel, Empresa>(obj);
            BeginTransaction();
            _empresaService.Update(empresa);
            Commit();
        }

        public void Dispose()
        {
            _empresaService.Dispose();
        }

    }
}
