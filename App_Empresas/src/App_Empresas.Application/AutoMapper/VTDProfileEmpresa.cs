﻿using App_Empresas.Application.ViewModels;
using App_Empresas.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Application.AutoMapper
{
    public class VTDProfileEmpresa : Profile
    {
        public override string ProfileName => "ViewModelToDomainMappings";

        public VTDProfileEmpresa()
        {
            ConfigureEmpresa();
        }

        private void ConfigureEmpresa()
        {
            CreateMap<EmpresaViewModel, Empresa>()
                .ForMember(d => d.id, o => o.MapFrom(vm => vm.id))
                .ForMember(d => d.name, o => o.MapFrom(vm => vm.nomeEmpresa))
                .ForMember(d => d.description, o => o.MapFrom(vm => vm.descricao))
                .ForMember(d => d.tipo, o => o.MapFrom(vm => vm.tipo));
        }
    }
}
