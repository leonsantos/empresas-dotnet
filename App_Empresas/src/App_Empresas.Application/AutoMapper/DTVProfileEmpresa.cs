﻿using App_Empresas.Application.ViewModels;
using App_Empresas.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Application.AutoMapper
{
    public class DTVProfileEmpresa : Profile
    {
        public override string ProfileName => "DomainToViewModelMappings";

        public DTVProfileEmpresa()
        {
            ConfigureEmpresa();
        }

        private void ConfigureEmpresa()
        {
            CreateMap<Empresa, EmpresaViewModel>()
                .ForMember(vm => vm.id, o => o.MapFrom(d => d.id))
                .ForMember(vm => vm.nomeEmpresa, o => o.MapFrom(d => d.name))
                .ForMember(vm => vm.descricao, o => o.MapFrom(d => d.description))
                .ForMember(vm => vm.tipo, o => o.MapFrom(d => d.tipo));
        }
    }
}
