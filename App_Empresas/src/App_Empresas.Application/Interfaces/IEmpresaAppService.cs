﻿using App_Empresas.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Application.Interfaces
{
    public interface IEmpresaAppService :IDisposable
    {
        void Add(EmpresaViewModel obj);
        EmpresaViewModel GetById(Guid id);
        IEnumerable<EmpresaViewModel> GetAll();
        IEnumerable<EmpresaViewModel> GetAll_FilterByNameType(string name, string tipo);
        void Update(EmpresaViewModel obj);
        void Remove(Guid id);
    }
}
