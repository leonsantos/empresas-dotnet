﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Application.ViewModels
{
    public class EmpresaViewModel
    {
        public EmpresaViewModel()
        {
            id = Guid.NewGuid();
        }
        public Guid id { get; set; }
        public string nomeEmpresa { get; set; }
        public string descricao { get; set; }
        public string tipo { get; set; }
    }
}
