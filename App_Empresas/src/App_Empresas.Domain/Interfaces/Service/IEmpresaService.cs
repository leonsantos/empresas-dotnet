﻿using App_Empresas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Domain.Interfaces.Service
{
    public interface IEmpresaService : IDisposable
    {
        void Add(Empresa obj);
        Empresa GetById(Guid id);
        IEnumerable<Empresa> GetAll();
        IEnumerable<Empresa> GetAll_FilterByNameType(string name, string tipo);
        void Update(Empresa obj);
        void Remove(Guid id);
    }
}
