﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Domain.Entities
{
    public class Empresa
    {
        public Empresa()
        {
            id = Guid.NewGuid();
        }

        public Guid id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string tipo { get; set; }
    }
}
