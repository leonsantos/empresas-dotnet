﻿using App_Empresas.Domain.Entities;
using App_Empresas.Domain.Interfaces.Respository;
using App_Empresas.Domain.Interfaces.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Domain.Services
{
    public class EmpresaService : IEmpresaService
    {
        public readonly IEmpresaRepository _empresaRepository;

        public EmpresaService(IEmpresaRepository empresaRepository)
        {
            _empresaRepository = empresaRepository;
        }

        public void Add(Empresa obj)
        {
            _empresaRepository.Add(obj);
        }

        public IEnumerable<Empresa> GetAll()
        {
            return _empresaRepository.GetAll();
        }

        public IEnumerable<Empresa> GetAll_FilterByNameType(string name, string tipo)
        {
            return _empresaRepository.GetAll_FilterByNameType(name, tipo);
        }

        public Empresa GetById(Guid id)
        {
            return _empresaRepository.GetById(id);
        }

        public void Remove(Guid id)
        {
            _empresaRepository.Remove(id);
        }

        public void Update(Empresa obj)
        {
            _empresaRepository.Update(obj);
        }

        public void Dispose()
        {
            _empresaRepository.Dispose();
        }
    }
}
