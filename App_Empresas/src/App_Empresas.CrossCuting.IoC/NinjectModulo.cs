﻿using App_Empresas.Application.AppServices;
using App_Empresas.Application.Interfaces;
using App_Empresas.Domain.Interfaces.Respository;
using App_Empresas.Domain.Interfaces.Service;
using App_Empresas.Domain.Services;
using App_Empresas.Infra.Data.Context;
using App_Empresas.Infra.Data.Interfaces;
using App_Empresas.Infra.Data.Repositories;
using App_Empresas.Infra.Data.UoW;
using Ninject.Modules;

namespace App_Empresas.CrossCuting.IoC
{
    public class NinjectModulo : NinjectModule
    {
        public override void Load()
        {
            //UnitOfWork & ContextManager
            Bind<IContextManager>().To<ContextManager>();
            Bind<IUnitOfWork>().To<UnitOfWork>();

            //AppServices
            Bind<IEmpresaAppService>().To<EmpresaAppService>();

            //Services
            Bind<IEmpresaService>().To<EmpresaService>();

            //Repositories
            Bind<IEmpresaRepository>().To<EmpresaRepository>();
        }
    }
}
