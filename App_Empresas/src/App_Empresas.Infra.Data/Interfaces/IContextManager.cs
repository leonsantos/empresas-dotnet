﻿using App_Empresas.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Infra.Data.Interfaces
{
    public interface IContextManager
    {
        App_EmpresasContext GetContext();
    }
}
