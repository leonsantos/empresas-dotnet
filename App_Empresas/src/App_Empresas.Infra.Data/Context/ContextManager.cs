﻿using App_Empresas.Infra.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace App_Empresas.Infra.Data.Context
{
    public class ContextManager : IContextManager
    {
        private const string ContextKey = "ContextManager.Context";
        public App_EmpresasContext GetContext()
        {                    
            if (HttpContext.Current.Items[ContextKey] == null)
            {
                HttpContext.Current.Items[ContextKey] = new App_EmpresasContext();
            }

            return (App_EmpresasContext)HttpContext.Current.Items[ContextKey];       
        }
    }
}
