﻿using App_Empresas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Infra.Data.EntityConfig
{
    public class EmpresaConfiguration : EntityTypeConfiguration<Empresa>
    {
        public EmpresaConfiguration()
        {
            ToTable("Empresa");

            HasKey(e => e.id);

            Property(e => e.name)
                .HasColumnName("Name")
                .IsRequired();

            Property(e => e.description)
                .HasColumnName("Description");

            Property(e => e.tipo)
                .HasColumnName("Type")
                .IsRequired();
        }
    }
}
