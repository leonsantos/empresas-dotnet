﻿using App_Empresas.Domain.Entities;
using App_Empresas.Domain.Interfaces.Respository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Infra.Data.Repositories
{
    public class EmpresaRepository : RepositoryBase<Empresa>, IEmpresaRepository
    {
        public IEnumerable<Empresa> GetAll_FilterByNameType(string name, string tipo)
        {
            var empresas = DbSet.Where(e => e.name.ToUpper() == name.ToUpper() && e.tipo.ToUpper() == tipo.ToUpper()).AsQueryable();

            return empresas;
        }
    }
}
