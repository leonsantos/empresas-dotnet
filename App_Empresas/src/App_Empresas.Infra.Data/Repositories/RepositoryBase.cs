﻿using App_Empresas.Domain.Interfaces.Respository;
using App_Empresas.Infra.Data.Context;
using App_Empresas.Infra.Data.Interfaces;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Empresas.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected DbSet<TEntity> DbSet;
        protected readonly App_EmpresasContext DbContext;

        public RepositoryBase()
        {
            var contextManager = ServiceLocator.Current.GetInstance<IContextManager>();
            DbContext = contextManager.GetContext();
            DbSet = DbContext.Set<TEntity>();
        }

        public void Add(TEntity obj)
        {
            DbSet.Add(obj);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet.AsEnumerable();
        }

        public TEntity GetById(Guid id)
        {
            return DbSet.Find(id);
        }

        public void Remove(Guid id)
        {
            var obj = GetById(id);
            DbSet.Remove(obj);
        }

        public void Update(TEntity obj)
        {
            //var entry = DbContext.Entry(obj);
            //DbSet.Attach(obj);
            //entry.State = EntityState.Modified;

            try
            {
                var entry = DbContext.Entry(obj);
                DbSet.Attach(obj);
                DbPropertyValues originalValues = null;

                if (entry.State != EntityState.Detached)
                {
                    originalValues = entry.OriginalValues.Clone();
                    DbSet.Attach(obj);
                    entry.State = EntityState.Modified;
                    entry.OriginalValues.SetValues(originalValues);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public void Dispose()
        {
            DbContext.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
